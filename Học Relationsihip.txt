* Tài liệu tham khảo: https://viblo.asia/p/tim-hieu-ve-cac-relationships-trong-laravel-oOVlYknrK8W

* Relationship:(namespace App)

+ One-one:
. Liên kết 1 user với 1 avatar dùng hàm : $this->hasOne('App\Avatar');

.Lấy ra avatar của user có id là 1: $avatar = User::find(1)->avatar;(Sau khi mà liên kết ntn xong)

=>Lưu ý:Nó sẽ tự động match cột khóa ngoại(trong trường hợp này là user_id của bảng ava và id của bảng user)
	  Trong trường hợp khóa ngoại k đặt là user_id mà đặt tên khác thì: $this->hasOne('App\Avatar', 'foreign_key');
    	  Trong trường hợp tương tự là khóa chính không đặt là id thì:return $this->hasOne('App\Avatar', 'foreign_key', 'local_key');

.Inverse: * Có thể lấy user từ avatar bằng quá trình đảo ngược: 
	    (  public function user()
             {$this->belongsTo('App\User');})
	    * có thể truy vấn tương tự từ avatar này để truy vấn ra user thuộc về nó.

	     return $this->belongsTo('App\User', 'foreign_key của bảng liên kết đến - bảng user');


	    * Nếu như bạn không muốn map foreign key của bảng Avatar với cột id ở bảng User mà lại là 1 cột khác chả hạn thì tiếp tục thêm 1 tham số thứ 3 như sau.

	    return $this->belongsTo('App\User', 'foreign_key', 'other_key - khóa chính của bảng');


+ One-Many:

. Một liên kết với nhiều dùng hàm: return $this->hasMany('App\Post');

. Truy vấn tới nhiều posts thuộc 1 user :$user = App\User::find(1)->posts;

+ Many - Many:

. Một A có thể có nhiều B và Một B cũng có thể có nhiều A : return $this->belongsToMany('App\Order');

. VD: Lấy ra 1 product có  bao nhiêu order  : $orders = App\Product::find(1)->orders;

Many - many sẽ tự tạo 1 bảng trung gian ở giữa và nếu muốn thay đổi tên bảng này:
. return $this->belongsToMany('App\Order', 'product_order');

Custom 2 tên khóa chính của 2 bảng có thể dùng:
. return $this->belongsToMany('App\Order', 'product_order', 'khóa bảng A - không p bảng order', 'khóa chính của bảng order');

Ngược lại với ý đầu tiên: .return $this->belongsToMany('App\Product');

Lấy giá trị của bảng trung gian : Để truy cập đến các cột của bảng trung gian chúng ta sẽ sử dụng thuộc tính pivot

. $product = App\Product::find(1);

foreach($product->orders as $order)
{
    echo $order->pivot->created_at;
}

Theo mặc định thì Eloquent chỉ lấy các trường trung gian và created_at, update_at nếu chúng ta muốn lấy ra giá trị của một cột khác thì cần khai báo thêm như sau, giả sử chúng ta cần lấy thêm trường address:

. return $this->belongsToMany('App\Product')->withPivot('address');